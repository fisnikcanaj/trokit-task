import { Component } from '@angular/core';
import {DrawerSelectEvent} from "@progress/kendo-angular-layout";
import { Router} from "@angular/router";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'trokit';

  constructor(private router: Router) {
  }

  public items: Array<any> = [
    { text: 'Employee', icon: 'k-i-accessibility', link: '/employee', selected: true },
    { separator: true },
    { text: 'Projects', icon: 'k-i-track-changes-accept-all', link: '/projects' },
  ];

  public onSelect(ev: DrawerSelectEvent): void {
    this.router.navigate([ev.item.link]);
  }
}
