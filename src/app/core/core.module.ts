import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import {
  ApiService, EmployeeService, ProjectService,
} from './services';

@NgModule({
  imports: [
    CommonModule,

  ],
  providers: [
    ApiService,
    EmployeeService,
    ProjectService
  ],
  declarations: []
})
export class CoreModule { }
