import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ApiService } from './api.service';
import { Employee } from '../models';
import {tap} from "rxjs/operators";
import * as moment from "moment";

@Injectable()
export class EmployeeService {
  private apiLink = 'employee.json';
  constructor (
    private apiService: ApiService
  ) {}

  getEmployees(): Observable<Employee[]> {
    return this.apiService.get(this.apiLink).pipe(tap(e => {
    e.map((p: any) => {
        p.dateOfBirth = moment(p.dateOfBirth).format('MM/DD/YYYY');
        return p;
      });
    }))
  }

}
