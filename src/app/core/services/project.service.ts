import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ApiService } from './api.service';
import {Employee, Project} from '../models';
import {map, tap} from 'rxjs/operators';
import * as moment from "moment";

@Injectable()
export class ProjectService {
  private apiLink = 'projects.json'
  constructor (
    private apiService: ApiService
  ) {}

  getProjects(): Observable<Project[]> {
    return this.apiService.get(this.apiLink).pipe(tap(e => {
      e.map((p: any) => {
        p.startDate = moment(p.startDate).format('MM/DD/YYYY HH:mm');
        p.endDate = moment(p.endDate).format('MM/DD/YYYY HH:mm');
        return p;
      });
    }))
  }

}
