import {Project} from "./project.model";

export interface Employee {
  id: number;
  firstName: string;
  lastName: string;
  title: string;
  dateOfBirth: Date;
  projects: Project[];
}
