import { Component, OnInit } from '@angular/core';
import {Employee, EmployeeService, Project} from "../core";
import {AbstractControl, FormControl, FormGroup, Validators} from "@angular/forms";
import {tap} from "rxjs/operators";
import * as moment from 'moment';


@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss']
})
export class EmployeeComponent implements OnInit {
  public employees: Employee[];
  public employeeProjects: Project[];

  public form: FormGroup;

  constructor(private employeeService: EmployeeService) {
    this.form = new FormGroup({
      id: new FormControl('', ),
      firstName: new FormControl('', [Validators.required]),
      title: new FormControl('', [Validators.required]),
      lastName: new FormControl('', [Validators.required]),
      dateOfBirth: new FormControl('', [Validators.required]),
    });
  }

  ngOnInit(): void {
    this.getEmployee();
  }
  private getEmployee() {
    this.employeeService.getEmployees().subscribe(employees => {
      this.employees = employees;
    })
  }

  get id(): AbstractControl {
    return this.form.get('id') as AbstractControl;
  }

  get firstName(): AbstractControl {
    return this.form.get('firstName') as AbstractControl;
  }

  get title(): AbstractControl {
    return this.form.get('title') as AbstractControl;
  }

  get lastName(): AbstractControl {
    return this.form.get('lastName') as AbstractControl;
  }

 get dateOfBirth(): AbstractControl {
    return this.form.get('dateOfBirth') as AbstractControl;
  }

  public submit(): void {
    // console.log(this.form.value);
  }

  public selectedEmployee(selection: any): void {
    this.employeeProjects = selection.selectedRows[0].dataItem.projects;
    const filteredEmployee = {...selection.selectedRows[0].dataItem};
    delete filteredEmployee.projects;

    this.form.setValue(filteredEmployee);
  }

}
