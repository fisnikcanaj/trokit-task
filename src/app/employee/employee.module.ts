import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmployeeComponent } from './employee.component';
import {TabStripModule, TileLayoutModule} from "@progress/kendo-angular-layout";
import {GridModule} from "@progress/kendo-angular-grid";
import { EmployeeRoutingModule } from './employee-routing.module';
import {FormFieldModule, MaskedTextBoxModule, TextBoxModule} from "@progress/kendo-angular-inputs";
import {ReactiveFormsModule} from "@angular/forms";

import { LabelModule } from "@progress/kendo-angular-label";
import { InputsModule } from "@progress/kendo-angular-inputs";
import { DropDownsModule } from "@progress/kendo-angular-dropdowns";
import {ButtonsModule} from "@progress/kendo-angular-buttons";


@NgModule({
  declarations: [
    EmployeeComponent,

  ],
  imports: [
    CommonModule,
    TabStripModule,
    GridModule,
    EmployeeRoutingModule,
    FormFieldModule,
    TextBoxModule,
    ReactiveFormsModule,
    MaskedTextBoxModule,
    InputsModule,
    LabelModule,
    DropDownsModule,
    ButtonsModule,
    TileLayoutModule
  ]
})
export class EmployeeModule { }
