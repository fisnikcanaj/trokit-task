import { Component, OnInit } from '@angular/core';
import { Project, ProjectService} from "../core";
import {AbstractControl, FormControl, FormGroup, Validators} from "@angular/forms";
import { IntlService } from "@progress/kendo-angular-intl";
import {map, tap} from "rxjs/operators";
import * as moment from 'moment';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent implements OnInit {
  public  fullFormat = "dd/MMM/yyyy HH:mm";
  public projects: Project[];
  public startDateValue: Date = new Date();
  public endDateValue: Date = new Date;

  public form: FormGroup;

  constructor(private projectService: ProjectService, public intl: IntlService) {
    this.form = new FormGroup({
      id: new FormControl(''),
      name: new FormControl('', [Validators.required]),
      startDate: new FormControl('', [Validators.required]),
      endDate: new FormControl('', [Validators.required]),
      status: new FormControl('', [Validators.required]),
    });
  }

  ngOnInit(): void {
    this.getProject();
  }
  private getProject() {
    this.projectService.getProjects().pipe(tap(projects => {
      return projects.map(p => {
        p.endDate = new Date(moment(p.endDate).format('D MMM YYYY'));

        return p;

      })
    })).subscribe(projects => {
      this.projects = projects;
    })
  }

  get id(): AbstractControl {
    return this.form.get('id') as AbstractControl;
  }

  get name(): AbstractControl {
    return this.form.get('name') as AbstractControl;
  }

  get startDate(): AbstractControl {
    return this.form.get('startDate') as AbstractControl;
  }

  get endDate(): AbstractControl {
    return this.form.get('endDate') as AbstractControl;
  }

  get status(): AbstractControl {
    return this.form.get('status') as AbstractControl;
  }

  public submit(): void {
    console.log(this.form.value);
  }

  public selectedProject(selection: any): void {
    if (this.startDateValue != undefined) {
      this.startDateValue = new Date(selection.selectedRows[0].dataItem.startDate);
    }
    if (this.endDateValue != undefined) {
      this.endDateValue = new Date(selection.selectedRows[0].dataItem.endDate);
    }

    this.form.setValue(selection.selectedRows[0].dataItem);
  }
}
