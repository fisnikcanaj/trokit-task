import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectsComponent } from './projects.component';
import {LayoutModule} from "@progress/kendo-angular-layout";
import {FormFieldModule, TextBoxModule} from "@progress/kendo-angular-inputs";
import {ReactiveFormsModule} from "@angular/forms";
import { ProjectsRoutingModule } from "./projects-routing.module"

import { LabelModule } from "@progress/kendo-angular-label";
import { InputsModule } from "@progress/kendo-angular-inputs";
import { DropDownsModule } from "@progress/kendo-angular-dropdowns";
import {ButtonsModule} from "@progress/kendo-angular-buttons";
import {TabStripModule, TileLayoutModule} from "@progress/kendo-angular-layout";
import {GridModule} from "@progress/kendo-angular-grid";
import {DateTimePickerModule} from "@progress/kendo-angular-dateinputs";


@NgModule({
  declarations: [
    ProjectsComponent
  ],
  imports: [
    CommonModule,
    LayoutModule,
    FormFieldModule,
    TextBoxModule,
    ReactiveFormsModule,
    ProjectsRoutingModule,
    LabelModule,
    InputsModule,
    DropDownsModule,
    ButtonsModule,
    TabStripModule,
    TileLayoutModule,
    GridModule,
    DateTimePickerModule
  ]
})
export class ProjectsModule { }
